<?php

// error_reporting(E_ALL);

// ini_set('display_errors', 1);

function fourOhFour() {
	header('HTTP/1.0 404 Not Found'); die;
}

/**
 * If this page was hit directly, and it's not the facebook crawler
 * then redirect to the dynamic version of this page
 */
if(
	isset($_SERVER['HTTP_USER_AGENT']) &&
	isset($_SERVER['REQUEST_URI'])     &&
	strpos($_SERVER['HTTP_USER_AGENT'], 'facebookexternalhit') === false
) {

	$uri = $_SERVER['REQUEST_URI'];

	$uri = str_replace('?_escaped_fragment_=', '#!', $uri);

	header('Location: ' . $uri, true, 301); die;

}

/**
 * Validate input
 */
if(!isset($_GET['page']) || !is_string($_GET['page'])) {
	fourOhFour();
}

/**
 * Build path to snapshot
 */

$path = trim($_GET['page'], '?/');

$path = dirname(__FILE__) . '/snapshots/' . $path . '.html';

$path = realpath($path);

if($path === false) {
	fourOhFour();
}

/**
 * Serve static content
 */
echo file_get_contents($path);