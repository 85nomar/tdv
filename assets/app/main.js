/***********************************************************
 main.js for INTRAS, Temas de Vanguardia
 =======================================================
 Date: 08.07.2014
 Author: Novel Martinez
 Version: 1.6

 Description:
 JS File which defines the main Angular module "TDV"
 of this application.
 ***********************************************************/

// Creating an application module.

var tdv = angular.module("tdvApp", ['angularUtils.directives.dirDisqus','viewhead', 'ui.bootstrap']);
    /*
    .run(function($FB){
        //facebook app ID - Temas de Vanguardia
        $FB.init('1473262116245654');
    });
    */

// Configure the routing. The $routeProvider will be automatically injected into 
// the configurator.

tdv.config(
	function( $routeProvider, $locationProvider ){


		//mapping routes to render "Actions"
		$routeProvider
			.when(
				"/home",
				{
					action: "splash.home"
				}
			)
			.when(
				"/categories",
				{
					action: "standard.videos.categories"
				}
			)
			.when(
				"/videos/:categoryID",
				{
					action: "standard.videos.list"
				}
			)
            .when(
                "/videos/:categoryID/:videoID/related",
                {
                    action: "standard.videos.detail.relatedList"
                }
            )
			.when(
				"/videos/:categoryID/:videoID",
				{
					action: "standard.videos.detail.background"
				}
			)
			.when(
				"/videos/:categoryID/:videoID/intro",
				{
					action: "standard.videos.detail.intro"
				}
			)
			.when(
				"/videos/:categoryID/:videoID/medical-history",
				{
					action: "standard.videos.detail.medicalHistory"
				}
			)
			.when(
				"/about",
				{
					action: "standard.about"
				}
			)
                .when(
                "/contact",
                {
                    action: "standard.contact"
                }
            )
			.otherwise(
				{
					redirectTo: "/home"
				}
            )
        ;

        $locationProvider.html5Mode(false).hashPrefix("!");

	}
);
