(function( ng, app ) {
	
	"use strict";

	// I provide a repository for the categories.
	app.service(
		"categoryService",
		function( $q,$http, _ ) {


			// I get all of the categories.
			function getCategories() {

				var deferred = $q.defer();
				
				deferred.resolve( ng.copy( cache ) );

				return( deferred.promise );

			}


			// I get the category with the given ID.
			function getCategoryByID( id ) {

				var deferred = $q.defer();
				var category = _.findWithProperty( cache, "id", id );

				if ( category ) {

					deferred.resolve( ng.copy( category ) );

				} else {

					deferred.reject();

				}

				return( deferred.promise );

			}


			// ---------------------------------------------- //
			// ---------------------------------------------- //


			// Set up the categories data cache. For this tdv, we'll just use static data.
			var cache = [
               /* {
                    id: "administration",
                    name: "Administración",
                    description: "Contenidos que tocan aspectos generales de gestión relativos a la planificación, organización, dirección y optimización de recursos.  No abarcan desarrollo de competencias ni habilidades."
                }, */
                {
                    id: "quality",
                    name: "Calidad",
                    description: "Contenidos relativos a la planificación, organización, documentación y puesta en marcha de los sistemas de gestión de calidad. Pueden abarcar temáticas concernientes a la gestión por procesos y a la mejora continua.   "
                },
                {
                    id: "clients",
                    name: "Comercial y Gestión de Clientes",
                    description: "Contenidos relativos a las estrategias y mejores prácticas comerciales, así como a la optimización de los procesos de gestión de relaciones con los clientes."
                },
               {
                    id: "skills",
                    name: "Desarrollo de Habilidades",
                    description: "Contenidos relativos al desarrollo de competencias y habilidades específicas, tanto individuales como de equipos. "
                },
                {
                    id: "finance",
                    name: "Finanzas",
                    description: "Contenidos relacionados a la gestión y optimización de recursos financieros en las organizaciones."
                },
                {
                    id: "management",
                    name: "Gestión Humana",
                    description: "Contenidos relativos a las mejores prácticas, herramientas y estrategias de gestión de personas."
                },
                {
                    id: "market",
                    name: "Mercadeo y Comunicaciones",
                    description: "Contenidos relativos a las herramientas, metodologías y estrategias de Marketing y Comunicaciones."
                },
                /*{
                    id: "projects",
                    name: "Proyectos",
                    description: "Contenidos que abarcan todas las metodologías, técnicas, tecnologías, modelos y herramientas que dan soporte al proceso de gestión de proyectos."
                },*/
               /* {
                    id: "logistic",
                    name: "Operaciones, Logística y Distribución",
                    description: "Contenidos relativos a la gestión de los procesos operativos, logísticos, abastecimiento y distribución en las organizaciones. "
                }, */
                {
                    id: "informatics",
                    name: "Tecnología de Información y Sistemas",
                    description: "Contenidos que abarcan la gestión de tecnologías de información y sistemas."
                },
                {
                    id: "gurus",
                    name: "Grandes Speakers",
                    description: "Videos de entrevistas a conferencistas que han expuesto en grandes eventos de INTRAS y que son importantes autoridades globales de la gerencia moderna y el mundo de los negocios."
                },
                {
                    id: "others",
                    name: "Otros",
                    description: "Videos de variado contenido no relativos al mundo de los negocios."
                }

			];


			// ---------------------------------------------- //
			// ---------------------------------------------- //


			// Return the public API.
			return({
				getCategories: getCategories,
				getCategoryByID: getCategoryByID
			});


		}
	);

})( angular, tdv );