(function( ng, app ) {
	
	"use strict";

	// I provide a repository for the videos.
	app.service(
		"videoService",
		function( $q,$http, _ ) {


            function getVideos() {

                var deferred = $q.defer();
                var videos = cache;

                if (videos) {
                    deferred.resolve(ng.copy(videos));
                } else {
                    deferred.reject();
                }
                return( deferred.promise );
            }

            // I get the video with the given ID.
            function getvideoByID(id) {

                var deferred = $q.defer();
                var video = _.findWithProperty(cache, "id", id);

                if (video) {
                    deferred.resolve(ng.copy(video));
                } else {
                    deferred.reject();
                }
                return( deferred.promise );
            }


            // I get the videos in the given category.
            function getvideosByCategoryID(categoryID) {

                var deferred = $q.defer();
                var videos = _.filterWithProperty(cache, "categoryID", categoryID);

                if (videos) {
                    deferred.resolve(ng.copy(videos));
                } else {
                    deferred.reject();
                }
                return( deferred.promise );
            }

             //TODO: FIX THIS!
            // I get a random video in the given category, less the given video.
            function getRandomvideoExcluding(categoryID, excludevideoID) {

                var deferred = $q.defer();
                var videos = _.filterWithProperty(cache, "categoryID", categoryID);

                if (videos) {

                    var index = _.random(0, ( videos.length - 1 ));

                    while (videos[ index ].id === excludevideoID) {
                        index = _.random(0, ( videos.length - 1 ));
                    }

                    deferred.resolve(ng.copy(videos[ index ]));

                } else {
                    deferred.reject();
                }
                return( deferred.promise );
            }

            //Sending a video Code through the Youtube API and gets all Info about it
            function getYoutubeItems(code)
            {

                //google access key
                var key = 'AI39si5fn8PdIgy3f9GbdNZTTTDBx8uF-ipXGFPVyQf_Nf2si9SstYQwVv76LYwJM79jSgmdZQWeBrJh1ca8QQT2TbjKcbNWcA';
                var request = 'http://gdata.youtube.com/feeds/api/videos?q='+code+'&key='+key+'&alt=jsonc&v=2';
                var result = null;
                $.ajax({
                    url: request,
                    type: 'get',
                    async: false,
                    success: function(data) {
                        result = data.data.items[0];
                        //console.log(data.data.items[0]);
                    }
                });
                return result;
            }



            // ---------------------------------------------- //
            // ---------------------------------------------- //


            // Set up a collection of constants for our video categories. Normally, this would be managed
            // by the server-side relational database
            var categories = {
                clients: 'clients',
                administration: "administration",
                quality: "quality",
                skills: 'skills',
                finance: 'finance',
                management: 'management',
                market: 'market',
                projects: 'projects',
                logistic: 'logistic',
                informatics: 'informatics',
                gurus: 'gurus',
                others: 'others'
            };


            // Set up a collection of speakers.
            var speakers = {
                ajadad: "Alejandro Jadad",
                amartinez: "Alberto Martínez",
                avilches: "Antonio Vilches",
                avmbrusco: "Ana Victoria Mari Brusco",
                czamora: "Carlos Zamora",
                ebgueto: "Eugenio Belinchón Güeto",
                fcasalins: "Francisco Casalins",
                fjgalan: "Francisco Javier Galán",
                frios: "Frances Ríos",
                gbiasotti: "Gustavo Biasotti",
                gking: "Gustavo King",
                jhernandez: "Jesús Hernandez",
                jzelaya: "Julio Zelaya",
                ljdonaher: "Lino Jorge Donaher",
                mchu: "Manuel Chú",
                mgoldsmith: "Marshall Goldsmith",
                mmorales: "Mario Morales",
                mvarela: "Maryam Varela",
                pgonzalo: "Pablo Gonzalo",
                rcanamero: "Roberto Cañamero",
                rfernandez: "Rodrigo Fernández",
                smoschini: "Silvina Moschini",
                tkelley: "Tom Kelley"

            };

            // Set up the videos data cache. For this app, "static data".

            //set path where to find the portraits and videos
            var imgpath = "assets/app/img/pics/";
            var videopath = "assets/downloads/videos/";

            //set image format
            var imgformat = ".jpg";

            var cache = [
                   /* {
                        id: 1,
                        categoryID: categories.clients,
                        speaker: speakers.rcanamero,
                        filesName: 'rcanamero-1',
                        intro: 'En los mercados actuales exigentes, cambiantes y competitivos, las fuerzas de ventas constituyen el factor vital para obtener la participación con las marcas y líneas de productos. Los gestores de ventas (directores, gerentes, jefes, supervisores de ventas) constituyen un recurso clave en la sostenibilidad organizacional pues al final de cuentas son los que guían a los vendedores hacia el logro de los resultados e implementan tácticamente la estrategia diseñada por la empresa.',
                        descriptionShort: 'En este video, el facilitador Roberto Cañamero, experto en gestión efectiva de la fuerza de ventas, nos presenta las claves básicas para gestionar exitosamente equipos comerciales.',
                        items: getYoutubeItems('TtmzLeEMC60'),
                        article: '',
                        events: ''
                    },*/
                    {
                        id: 2,
                        categoryID: categories.management,
                        speaker: speakers.pgonzalo,
                        filesName: 'pgonzalo-2',
                        intro: 'La creciente exigencia de la competitividad por la globalización y por la inestabilidad de los mercados ha generado profundos cambios en la tradicional relación empleador/empleado. Hoy día todas las capacidades, puntos de vista y las energías son necesarias para lograr que las organizaciones sobrevivan y prosperen en los mercados. Esto solo se obtiene a través de lograr tener flexibilidad y, al mismo tiempo, con un muy elevado compromiso por parte de los empleados. Éstas y otras circunstancias de similar trascendencia son las que han hecho que la comunicación interna sea uno de los temas que actualmente más interés despierta entre los directivos. La comunicación interna se posiciona como una herramienta de importacia para mejorar el grado de implicación y compromiso empresarial de los empleados en las empresas.',
                        descriptionShort: 'En este video, el facilitador Pablo Gonzalo experto en Comunicación Interna, nos habla de cuáles son las claves para lograr que una comunicación interna sea eficaz, considerando a los empleados como los protagonistas del relato que queremos comunicar, más que verlos como un público.',
                        items: getYoutubeItems('TtmzLeEMC60'),
                        article: '',
                        events: ''
                    },
                    /*{
                        id: 3,
                        categoryID: categories.administration,
                        speaker: speakers.ebgueto,
                        filesName: 'ebgueto-3',
                        intro: 'Hoy día las empresas se enfrentan a la ausencia de certeza y el reto para su dirección es determinar cuánta incertidumbre se puede aceptar mientras se esfuerzan en incrementar el valor para sus diferentes grupos de interés. La incertidumbre implica riesgos y oportunidades y posee el potencial de erosionar o aumentar el valor. La gestión de riesgos corporativos permite a la dirección tratar eficazmente la incertidumbre y sus riesgos y oportunidades asociados, mejorando así la capacidad de generar valor.',
                        descriptionShort: 'El facilitador Eugenio Belinchón Gueto, experto en gestión de riesgos corporativos, nos amplía sobre el tema.',
                        items: getYoutubeItems('TtmzLeEMC60'),
                        article: '',
                        events: ''
                    },
                    {
                        id: 4,
                        categoryID: categories.skills,
                        speaker: speakers.gbiasotti,
                        filesName: 'gbiasotti-4',
                        intro: 'El concepto de negociación, abarca todos los ámbitos de la interacción entre personas ya que no solo se está negociando cuando hay un contrato de por medio o temas económicos a resolver, la vida es una negociación permanente y en ese sentido, una variedad de habilidades negociadoras son importantes para desempeñarse con éxito. La mayoría de los problemas que afrontan diariamente los profesionales o ejecutivos se resuelven a través de procesos de negociación, ya sea dentro de las empresas o con clientes y proveedores. De hecho, habilidades para negociar y comunicarse eficientemente son esenciales en todo tipo de negocios y relaciones, tanto personales como laborales.',
                        descriptionShort: 'En este video el facilitador Gustavo Biasotti, experto en negociación, nos habla sobre diferentes herramientas e ideas para lograr negociaciones exitosas.',
                        items: getYoutubeItems('TtmzLeEMC60'),
                        article: '',
                        events: ''
                    },*/
                    {
                        id: 5,
                        categoryID: categories.skills,
                        speaker: speakers.jzelaya,
                        filesName: 'jzelaya-5',
                        intro: 'Al día de hoy si hay una realidad que nadie se atreve a poner en duda o debatir es el hecho de que definitivamente vivimos en tiempos de incertidumbre y profundos cambios. Las organizaciones, como parte activa del entorno no están para nada ajenas a esta realidad. Todo lo contrario, ya sea para ser más competitivas y eficientes, para capitalizar sobre estos cambios o simplemente para sobrevivir, las organizaciones hoy más que nunca se han lanzado en profundos procesos y proyectos de transformación. Estos cambios van desde una reorganización de funciones hasta un cambio de plataforma tecnológica. O incluso una recomposición accionaria o una fusión. Lamentablemente, en muchas ocasiones los cambios organizativos colocan a las organizaciones en estados de tensión, caos y  confusión que impactan negativamente en su desempeño. Estas y muchas otras situaciones negativas por las que atraviesan algunas empresas tienen una causa común: una deficiente gestión de sus propias emociones y las de las personas que les rodean por parte de los líderes de la organización.',
                        descriptionShort: 'En este video, el facilitador Julio Zelaya, experto en gestión del cambio, nos presenta las claves para liderar y gestionar de manera exitosa el cambio en las organizaciones.',
                        items: getYoutubeItems('GmdM997v3HI'),
                        article: 'http://bit.ly/1iBAQEw',
                        events: ''
                    },
                    {
                        id: 6,
                        categoryID: categories.informatics,
                        speaker: speakers.czamora,
                        filesName: 'czamora-6',
                        intro: 'En la actualidad, la Tecnología de Información (TI) se ha convertido en un recurso estratégico para las organizaciones, no solo por generar nuevas formas de trabajo, sino también por provocar cambios significativos en la manera de hacer negocios. Es por esto que en los últimos años las organizaciones han destinado grandes cantidades de recursos humanos y financieros a la adquisición y/o diseño de equipos de cómputos, redes informáticas y manejadores de Bases de Datos, así como a la implementación de sistemas aplicativos (ERP’s, CRM’s, SCM’s, etc.) para soportar los procesos de ventas, marketing, producción, contables, financieros y administrativos, producción, administración, las relaciones con los clientes y la cadena de suministros, entre otros.',
                        descriptionShort:'En este video el facilitador Carlos Zamora, experto en Tecnologías de Información, nos habla sobre las claves para alinear la tecnología de información con los objetivos del Negocio.',
                        items: getYoutubeItems('8DQGiNJWJOU'),
                        article: '',
                        events: ''
                    },
                    /*{
                         //GET VIDEO
                         id: 7,
                         categoryID: categories.clients,
                         speaker: speakers.mmorales,
                         filesName: 'mmorales-7',
                         intro: 'Hoy la mayoría de las empresas compiten con mercados saturados, en donde todos los competidores ofrecen prácticamente los mismos productos o servicios con poca diferenciación y terminan en una guerra de precios que afecta los márgenes de utilidad de todas las empresas. En este contexto, las empresas necesitan innovar para diferenciarse de sus competidores y poder crecer rentablemente.',
                         descriptionShort:'En este video, el facilitador Mario Morales nos habla sobre la importancia de la innovación en el servicio para crear  momentos memorables que le den mayor valor a su marca, satisfacción a los clientes y utilidades a la empresa.',
                         items: getYoutubeItems('_KpeCk6NyZU'),
                         article: '',
                        events: ''
                     },  */
                    {
                        id: 8,
                        categoryID: categories.clients,
                        speaker: speakers.avilches,
                        filesName: 'avilches-8',
                        intro: 'Más que vender, las empresas hoy en día necesitan saber definir y desarrollar una estrategia exitosa de aporte de valor al cliente y basada en criterios de rentabilidad. Esto significa que las empresas y organizaciones deben saber optimizar la gestión de las relaciones cliente - proveedor, con un doble objetivo. Por una parte fidelizar a sus clientes claves o estratégicos en base a no romper sus expectativas y por otra el de buscar la rentabilidad de sus proyectos y sus acciones de venta. Captar y retener clientes, en un mercado cada vez más complejo y competitivo, es el primer requisito para cualquier empresa que desee lograr el éxito. Si bien es aceptado que todos los clientes son importantes, existe siempre un 20% de estos que aporta un 80% del volumen de negocio. A estos, los denominamos los Clientes Claves o “Key Accounts” y su gestión puede significar la diferencia entre una empresa que prospera y otra que simplemente lucha para sobrevivir.',
                        descriptionShort:'En este video el facilitador Antonio Vilches, experto en gestión de Cuentas Claves, nos habla sobre los principios básicos para la gestión de exitosa de los clientes estratégicos.',
                        items: getYoutubeItems('QmNxusrK98A'),
                        article: '',
                        events: ''
                    },
                   /*
                   {
                        id: 9,
                        categoryID: categories.skills,
                        speaker: speakers.mvarela,
                        filesName: 'mvarela-9',
                        intro: 'La asertividad se refiere a la comprensión y mejora de nuestras relaciones sociales. Es una conducta que permite a una persona actuar con base a sus intereses más importantes, defenderse sin ansiedad, expresar cómodamente sentimientos honestos o ejercer los derechos personales, sin negar los derechos de otros.',
                        descriptionShort:'La asertividad en el mundo laboral juega un rol significativo. Hoy ya no sirve que una persona responda a su jefe sobre un mal resultado argumentando que no se le entregaron bien las instrucciones, por ejemplo, sino que la persona tiene la iniciativa de solicitar la información que le falta, exigir anticipadamente aquello que necesita para realizar las funciones y tareas que le corresponden.',
                        items: getYoutubeItems('fzKrPIN8z8I'),
                        article: '',
                        events: ''
                    }, */

                    {
                        id: 10,
                        categoryID: categories.others,
                        speaker: speakers.ajadad,
                        filesName: 'ajadad-10',
                        intro: '',
                        descriptionShort:'En este video Alejandro Jadad, experto en Informática de la Salud, nos presenta un  nuevo concepto de salud que se basa en la habilidad que tenemos los individuos y las comunidades para adaptarnos y para auto gestionar los desafíos físicos, mentales o sociales que nos presenta la vida. Este nuevo concepto de salud nos permite ser saludables a pesar de tener enfermedades.',
                        items: getYoutubeItems('fzKrPIN8z8I'),
                        article: '',
                        events: ''
                    },
                   {
                        id: 11,
                        categoryID: categories.skills,
                        speaker: speakers.frios,
                        filesName: 'frios-11',
                        intro: 'Hoy día el conocimiento técnico no es suficiente para impactar y persuadir a clientes, compañeros, colaboradores y stakeholders. Es por eso que todo profesional que quiera ser exitoso debe transformarse en un comunicador de influencia. A la hora de liderar un equipo, un comunicador de influencia tiene el poder de conectar a su gente con la meta de la empresa, llevarlos a entender cómo su labor impacta los resultados económicos esperados, pero sobre todo le permite generar compromiso. Comunicarnos con orden, con claridad, con entusiasmo, con persuasión; en resumidas cuentas con eficacia, no es un lujo sino una necesidad.',
                        descriptionShort:'La facilitadora Frances Ríos nos habla en este video sobre algunas de las numerosas técnicas, tips y herramientas que han "democratizado" la facilidad para hablar en público y vencer el miedo escénico, transformando lo que antes se veía como un don innato en una habilidad adquirible y por ende mejorable.',
                       items: getYoutubeItems('AJ0i7QrL94M'),
                        article: 'http://bit.ly/TDWKez',
                        events: ''
                    },
                    {
                        id: 12,
                        categoryID: categories.finance,
                        speaker: speakers.mchu,
                        filesName: 'mchu-12',
                        intro: 'En un entorno lleno de incertidumbre, en donde el éxito de los resultados de la toma de decisiones son medidos por indicadores financieros, los ejecutivos tienen que estar preparados para justificar y medir financieramente sus decisiones, así como cuantificar su contribución a la empresa. Esto se debe a que si bien hoy día se valoran aspectos como qué tan efectivo es su estilo de gerencia o qué tan innovadoras son sus ideas, al final del camino su desempeño final será medido por la rentabilidad que pueda haber obtenido para los inversionistas. En la actualidad todavía son muy escasos los directivos, gerentes, profesionales y empresarios que tienen un conocimiento mínimo de finanzas que le permitan entender cómo se crea valor en un mercado competitivo y cambiante. No obstante es su deber y responsabilidad conocer de dónde proviene el valor de su empresa, entender cuáles son los inductores estratégicos que construyen el valor y los que lo destruyen.',
                        descriptionShort:'En este video el facilitador y experto en finanzas, Manuel Chú, nos habla sobre cuáles son los fundamentos de finanzas que debe conocer todo ejecutivo no financiero.',
                        items: getYoutubeItems('WeIBxwzv8'),
                        article: '',
                        events: ''
                    },
                    {
                        id: 13,
                        categoryID: categories.market,
                        speaker: speakers.smoschini,
                        filesName: 'smoschini-13',
                        intro: 'Hemos pasado de la Era de la Información a la Era del Exceso de Información. Hoy día los clientes son cada vez más difíciles de segmentar. El mundo digital ha provocado que se tengan que configurar nuevas y complejas estrategias en las que hay que entender y comprender a fondo cuál es el comportamiento del nuevo consumidor, cómo funcionan los canales digitales, cómo dialogan entre ellos y cómo las comunicaciones y acciones de Marketing completamente bidireccionales se complementan para maximizar los presupuestos de las marcas y de las empresas. ',
                        descriptionShort:'En este video la experta en Marketing Digital, Silvina Moshini, nos habla sobre cuáles son las claves del éxito empresarial en esta era del consumidor 2.0.',
                        items: getYoutubeItems('4836iC_F8Hs'),
                        article: 'http://bit.ly/TpiH0n',
                        events: ''
                    },
                    {
                        id: 14,
                        categoryID: categories.market,
                        speaker: speakers.jhernandez,
                        filesName: 'jhernandez-14',
                        intro: 'Hoy en día es todo un reto capitalizar el potencial en la Web 2.0 y las tecnologías móviles para impulsar los resultados de negocio y mejorar la relación con las audiencias clave de la empresa. Se torna imperante encontrar nuevas alternativas para reducir costos, posibilidades de mejorar los niveles de servicio, capacidades de fidelizar los clientes y, sobre todo, de explorar nuevas oportunidades de negocio y capitalizar nuevos mercados.',
                        descriptionShort:'En este video Jesús Hernández, Phd, experto en Mobile Marketing, nos habla de las nuevas oportunidades que presenta el mobile marketing y la correcta integración del mix de medios para sacar provecho  a las oportunidades que presenta la Web 2.0. ',
                        items: getYoutubeItems('L6QPCGdaipk'),
                        article: '',
                        events: ''
                    },
                    {
                        id: 15,
                        categoryID: categories.skills,
                        speaker: speakers.mvarela,
                        filesName: 'mvarela-15',
                        intro: 'El conflicto es parte inherente de nuestra naturaleza y está subyacente en todo tipo de relación humana. El conflicto surge a partir de visiones diferentes del mundo real, las mismas que a su vez ocurren, porque están basadas en valores, ideologías, creencias y aprendizajes diversos. De ahí que el estudio de lo que es un conflicto, las variables que intervienen en él y cómo contribuyen a éste resulta de necesidad suprema para comprender la forma cómo debemos mediar, conciliar o solucionarlos. Los conflictos en la organización no son negativos de por sí. De hecho, bien gestionados, estos pueden ser saludables y constructivos ya que por lo regular son fuente de oportunidades de mejora. Lo que sí es muy negativo es que los ejecutivos gestionen mal los conflictos que día a día surgen en la organización, no solo fallando a la hora de solucionarlos, sino muchas veces dejando que estos asuman dimensiones inmanejables.',
                        descriptionShort:'En este video, la facilitadora Maryam Varela, experta en inteligencia emocional, nos habla sobre las claves para la gestión exitosa de conflictos en las organizaciones.',
                        items: getYoutubeItems('NMhvhJ6DZ_4'),
                        article: 'http://bit.ly/1wFrSc9',
                        events: ''
                    },
                    {
                        id: 16,
                        categoryID: categories.clients,
                        speaker: speakers.rfernandez,
                        filesName: 'rfernandez-16',
                        intro: 'Hoy día la mayoría de las organizaciones se desenvuelven en mercados y sectores poco diferenciados, donde los productos y/o servicios son prácticamente idénticos y en los cuales, como resultado y por lo regular, la competencia se centra en el factor precio. Esto, combinado con el surgimiento de un consumidor cada vez más informado y empoderado, crea un entorno competitivo cada vez más retador para las empresas. Bajo esta retadora realidad, las organizaciones vanguardistas han asumido el factor servicio como un elemento clave de competitividad. Y dentro de este factor nada tiene más impacto en diferenciar, fidelizar y retener como una gestión exitosa e innovadora de la experiencia del cliente. El motivo es muy sencillo: mientras las características de un producto o incluso un servicio pueden ser fácilmente replicables, las emociones, gratificaciones y satisfacciones que vive un cliente con una empresa, servicio o marca no son tan fáciles de emular.',
                        descriptionShort:'En este video, el facilitador Rodrigo Fernández, experto en gestión estratégica del servicio, nos presenta las herramientas clave para lograr una gestión óptima de la experiencia del cliente.',
                        items: getYoutubeItems('U6Bh7EWFMYE'),
                        article: 'http://bit.ly/1mltCiM',
                        events: 'http://www.intras.com.do/index.php?option=com_content&view=article&id=399:gestion-de-la-experiencia-del-cliente&catid=87&Itemid=91'
                    },
                    {
                        id: 17,
                        categoryID: categories.skills,
                        speaker: speakers.jzelaya,
                        filesName: 'jzelaya-17',
                        intro: '¿Desea una organización que trascienda, que logre resultados en el largo plazo sin perderse en el intento? La respuesta está en los valores... Los valores organizacionales son las guías de cómo una organización piensa y se comporta. Estos no son parte de la decoración de las paredes ni contenido de la memoria de la empresa, sino que solo son relevantes en la medida en que estos se “respiran y perspiran” en cultura en la empresa. En pocas palabras, estos valores definen su razón de ser e identifican cuales son las conductas correctas en la organización. Pero más aún, los valores a su vez son la mayor garantía de crecimiento de una organización, pues estos son lo que determinan las conductas de las personas “cuando nadie los ve”.',
                        descriptionShort:'En este video el facilitador Julio Zelaya, experto en gestión del cambio, nos habla sobre las bases fundamentales para liderar equipos utilizando los valores organizacionales como elemento referencial clave.',
                        items: getYoutubeItems('qPXSlrxqHo4'),
                        article: '',
                        events: ''
                    },
                     {
                        id: 18,
                        categoryID: categories.market,
                        speaker: speakers.avmbrusco,
                        filesName: 'avmbrusco-18',
                        intro: 'El retail o venta al detalle es el punto de encuentro donde el comprador y el producto se relacionan. ¿Cómo hacer que los productos se vean, diferencien, roten y generen rentabilidad acorde a los objetivos? ',
                        descriptionShort:'En este video, la facilitadora y experta en comercialización, Ana Victoria Mari Brusco, nos habla sobre las mejores prácticas y tendencias para una óptima gestión de venta al detalle.',
                        items: getYoutubeItems('HilI1gUX1WE'),
                        article: '',
                        events: ''
                    },
                    {
                        id: 19,
                        categoryID: categories.skills,
                        speaker: speakers.fjgalan,
                        filesName: 'fjgalan-19',
                        intro: 'Una de las principales características de las empresas de éxito de hoy es que cuentan con equipos empoderados y altamente comprometidos. Para que los líderes sientan la confianza requerida para otorgar ese poder a sus equipos es vital descubrir el potencial, en su mayor medida, de cada integrante del equipo. Por esa razón es responsabilidad de toda persona que tiene gente a su cargo o que ayuda a desarrollar a otras personas, conocer a profundidad las herramientas del Coaching Ejecutivo. ',
                        descriptionShort:'',
                        items: getYoutubeItems('fg97Fint2rI'),
                        article: 'http://bit.ly/UQ0Kth',
                        events: ''
                    },
                    {
                        //GET VIDEO
                        id: 20,
                        categoryID: categories.market,
                        speaker: speakers.ljdonaher,
                        filesName: 'ljdonaher-20',
                        intro: 'Las empresas de hoy día, tanto las tradicionales así como las que desarrollan su actividad en sectores emergentes, deben convertirse en perfectas comunicadoras eficaces promotoras de sus productos y claro está, de ellas mismas. Es por ello que el papel de la comunicación de Marketing cobra cada vez más importancia y esto solo se logra a través del conocimiento y manejo apropiado de las herramientas del mix de comunicación.',
                        descriptionShort:'En este video, el facilitador Lino Jorge, experto en Comunicaciones Integradas de Mercadeo, nos habla sobre las bases para lograr una efectiva comunicación en marketing en nuestras organizaciones.',
                        items: getYoutubeItems('tXTl9-WY6n0'),
                        article: '',
                        events: ''
                    },
               /* {
                    //GET VIDEO
                    id: 21,
                    categoryID: categories.logistic,
                    speaker: speakers.fcasalins,
                    filesName: 'ljdonaher-21',
                    intro: 'En los últimos veinte años las empresas han aumentado progresivamente su dependencia de los recursos externos, incrementando el conjunto de productos y servicios que pasan a ser realizados con recursos propios a ser comprados con recursos ajenos. Esta evolución demanda que la función de compras tome un mayor protagonismo y responsabilidad en el éxito del negocio de una empresa.',
                    descriptionShort:'A través de este video, el facilitador Francisco Casalins, experto en gestión de compras, nos muestra las mejores prácticas para la gestión estratégica de compras a fin de lograr que éstas sean un importante catalizador empresarial.',
                    items: getYoutubeItems('_KpeCk6NyZU'),
                    article: '',
                    events: ''
                }, */
                {
                    id: 22,
                    categoryID: categories.quality,
                    speaker: speakers.amartinez,
                    filesName: 'amartinez-22',
                    intro: 'Todas las actividades de la organización, desde la planificación de las compras hasta la atención de una reclamación, pueden y deben considerarse como procesos. Para operar de una manera eficaz, las organizaciones tienen que identificar y gestionar numerosos procesos interrelacionados y que interactúan entre ellos. La Gestión por Procesos es la forma de gestionar toda la organización basándose en los procesos. La meta es disponer de los instrumentos necesarios para conseguir optimizar el rendimiento de la organización y conseguir una orientación al cliente de forma rentable.',
                    descriptionShort:'En este video el facilitador Alberto Martínez, Especialista en la Gestión por Procesos, nos presenta cuáles son los principios para una gestión por procesos exitosa.',
                    items: getYoutubeItems('gHQJVOXR3R8'),
                    article: 'http://bit.ly/1m4zBOD',
                    events: ''
                },
                {
                     //GET VIDEO
                     id: 23,
                     categoryID: categories.market,
                     speaker: speakers.gking,
                     filesName: 'gking-23',
                     intro: 'Toda empresa, sin importar su tamaño o el sector en que se desenvuelva, debe establecer de manera clara y precisa las estrategias de Marketing a seguir, esto es aquellas acciones, ofensivas o defensivas, que le permitan crear una posición dentro de la industria y a la vez obtener un importante rendimiento sobre la inversión. Una buena estrategia de marketing integrará los objetivos de Marketing de la empresa, las políticas, las secuencias de acción (tácticas) dentro de un todo coherente, con el objetivo final de poner la empresa en posición de llevar a cabo su misión de forma efectiva y eficiente. Estas estrategias de Marketing desempeñan un papel fundamental como frontera entre la empresa y sus clientes, así como con sus competidores. De ahí la importancia de que todo profesional del área de Marketing cuente con las herramientas, domine los conceptos y tenga los conocimientos necesarios para poder implantarlas exitosamente.',
                     descriptionShort:'En este video el facilitador Gustavo King nos habla sobre las bases fundamentales para lograr una gestión estratégica de marketing que permitan alcanzar los objetivos propuestos por la alta dirección.',
                     items: getYoutubeItems('O8TZT9I8izk'),
                     article: '',
                     events: ''
                },/*
                {
                     //GET VIDEO
                     id: 24,
                     categoryID: categories.finance,
                     speaker: speakers.mchu,
                     filesName: 'mchu-24',
                     intro: '',
                     descriptionShort:'',
                     items: getYoutubeItems('_KpeCk6NyZU'),
                     article: 'http://bit.ly/TpiH0n',
                     events: ''
                }
                */
                {
                    //GET VIDEO
                    id: 25,
                    categoryID: categories.gurus,
                    speaker: speakers.tkelley,
                    filesName: 'tkelley-25',
                    intro: 'Hoy día, la convicción generalizada de que las organizaciones que aspiren a ser exitosas y competitivas deben ser dinámicas, innovadoras, creativas y proactivas es ya un hecho irrefutable. Sin embargo, apenas un limitado porcentaje de organizaciones, incluyendo a las que son líderes de sus respectivos sectores, alcanza el estatus de empresa “emblemática” o “icónica”. Más paradójico aún resulta el hecho de que apenas un puñado de las que sí alcanzan este estatus logran permanecer así en el tiempo. La dura realidad es que casi todas las empresas, incluso las que son grandes y rentables, están en una especie de “letargo eterno” sustentado en el statu quo, el crecimiento orgánico, leves mejoras continuas y, en los mejores casos, alguna que otra adaptación o innovación incremental... De igual forma, si analizamos su clima interno, nos encontramos con que la inmensa mayoría de personas están en “piloto automático”, haciendo labores cotidianas y emocionalmente desconectadas de la misión de la organización. La pregunta sería entonces: ¿Cómo salimos de esta situación? Las organizaciones que aspiren a dominar sus mercados deben renovarse constantemente. Para ello, necesitan desarrollar un ambiente interno que facilite y, sobre todo, estimule la aportación y la implementación de las ideas por parte de cualquier persona y en cualquier momento. En otras palabras, se necesita un entorno que promueva la creatividad, la colaboración y el empoderamiento en sus equipos.',
                    descriptionShort:'',
                    items: getYoutubeItems('3HPSeARYSho'),
                    article: 'En este video, Tom Kelley,  autoridad global sobre el tema de innovación, aborda las claves sobre cómo potenciar la creatividad, la colaboración y el empoderamiento en los equipos organizacionales y en las empresas como un todo, de manera que se permita a los líderes construir organizaciones capaces de afrontar los más grandes retos y solucionar los más grandes problemas.',
                    events: ''
                }/*,
                {
                    //GET VIDEO
                    id: 26,
                    categoryID: categories.gurus,
                    speaker: speakers.mgoldsmith,
                    filesName: 'tkelly-25',
                    intro: '',
                    descriptionShort:'',
                    items: getYoutubeItems('_KpeCk6NyZU'),
                    article: '',
                    events: ''
                }  */


            ];


            // ---------------------------------------------- //
            // ---------------------------------------------- //


            // Return the public API.
            return({
                getVideos: getVideos,
                getvideoByID: getvideoByID,
                getvideosByCategoryID: getvideosByCategoryID
                //getRandomvideoExcluding: getRandomvideoExcluding,
            });


        }
	);

})( angular, tdv );