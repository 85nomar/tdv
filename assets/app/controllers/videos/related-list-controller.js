(function( ng, app ){

	"use strict";

	app.controller(
		"videos.RelatedListController",
		function( $scope, $location, $q, requestContext, categoryService, videoService, _, $http ) {


			// --- Define Controller Methods. ------------------- //


			// I apply the remote data to the local view model.
			function applyRemoteData( category, videos ) {

				$scope.category = category;
				$scope.videos = _.sortOnProperty( videos, "name", "asc" );

				$scope.setWindowTitle( category.name );
				
			}


			// I load the remote data from the server.
			function loadRemoteData() {

				$scope.isLoading = true;

				var promise = $q.all(
					[
						categoryService.getCategoryByID( $scope.categoryID ),
						videoService.getvideosByCategoryID( $scope.categoryID )
					]
				);

				promise.then(
					function( response ) {

						$scope.isLoading = false;

						applyRemoteData( response[ 0 ], response[ 1 ] );

					},
					function( response ) {

						// The category couldn't be loaded for some reason - possibly someone hacking with the URL. 
						$location.path( "/videos" );

					}
				);

			}


			// --- Define Scope Methods. ------------------------ //


			// ...


			// --- Define Controller Variables. ----------------- //


			// Get the render context local to this controller (and relevant params).
			var renderContext = requestContext.getRenderContext( "standard.videos.list", "categoryID" );

			
			// --- Define Scope Variables. ---------------------- //


			// Get the ID of the category.
			$scope.categoryID = requestContext.getParam( "categoryID" );

			// I flag that data is being loaded.
			$scope.isLoading = true;

			// I am the category and the list of videos that are being viewed.
			$scope.category = null;
			$scope.videos = null;

			// The subview indicates which view is going to be rendered on the page.
			$scope.subview = renderContext.getNextSection();


			// --- Bind To Scope Events. ------------------------ //


			// I handle changes to the request context.
			$scope.$on(
				"requestContextChanged",
				function() {

					// Make sure this change is relevant to this controller.
					if ( ! renderContext.isChangeRelevant() ) {

						return;

					}

					// Get the relevant route IDs.
					$scope.categoryID = requestContext.getParam( "categoryID" );

					// Update the view that is being rendered.
					$scope.subview = renderContext.getNextSection();

					// If the relevant IDs have changed, refresh the view.
					if ( requestContext.hasParamChanged( "categoryID" ) ) {

						loadRemoteData();

					}

				}
			);


			// --- Initialize. ---------------------------------- //


			// Set the interim title.
			$scope.setWindowTitle( "Loading Related List" );

			// Load the "remote" data.
			loadRemoteData();

            //Set the ready status for the page as soon as this controller is finished
            $scope.status = 'ready';
		}
	);

})( angular, tdv );