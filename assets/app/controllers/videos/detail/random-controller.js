(function( ng, app ){

	"use strict";

	app.controller(
		"videos.detail.RandomController",
		function( $scope,$http, requestContext, videoService, _ ) {


			// --- Define Controller Methods. ------------------- //


			// I apply the remote data to the local view model.
			function applyRemoteData( video ) {

				$scope.video = video;

			}


			// I load the "remote" data from the server.
			function loadRemoteData() {

				$scope.isLoading = true;

				var promise = videoService.getRandomvideoExcluding( $scope.categoryID, $scope.videoID );

				promise.then(
					function( response ) {

						$scope.isLoading = false;

						applyRemoteData( response );

					},
					function( response ) {

						$scope.openModalWindow( "error", "For some reason we couldn't load a random video. Try refreshing your browser." );

					}
				);

			}


			// --- Define Scope Methods. ------------------------ //


			// ...


			// --- Define Controller Variables. ----------------- //


			// Get the render context local to this controller (and relevant params).
			var renderContext = requestContext.getRenderContext( "standard.videos.detail", [ "categoryID", "videoID" ] );

			
			// --- Define Scope Variables. ---------------------- //


			// Get the relevant route IDs.
			$scope.categoryID = requestContext.getParam( "categoryID" );
			$scope.videoID = requestContext.getParamAsInt( "videoID" );

			// I flag that data is being loaded.
			$scope.isLoading = true;

			// I hold the video to render.
			$scope.video = null;


			// --- Bind To Scope Events. ------------------------ //


			// I handle changes to the request context.
			$scope.$on(
				"requestContextChanged",
				function() {

					// Make sure this change is relevant to this controller.
					if ( ! renderContext.isChangeRelevant() ) {

						return;

					}

					// Get the relevant route IDs.
					$scope.categoryID = requestContext.getParam( "categoryID" );
					$scope.videoID = requestContext.getParamAsInt( "videoID" );

					// If the relevant ID has changed, refresh the view.
					if ( requestContext.haveParamsChanged( [ "categoryID", "videoID" ] ) ) {

						loadRemoteData();

					}

				}
			);


			// --- Initialize. ---------------------------------- //


			// Load the "remote" data.
			loadRemoteData();

            //Set the ready status for the page as soon as this controller is finished
            $scope.status = 'ready';
		}
	);

})( angular, tdv );