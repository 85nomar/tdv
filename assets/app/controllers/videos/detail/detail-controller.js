(function( ng, app ){

	"use strict";

	app.controller(
		"videos.detail.DetailController",
		function( $scope,$http, $location, $q, requestContext, categoryService, videoService, _ ) {


			// --- Define Controller Methods. ------------------- //


			// I apply the remote data to the local view model.
			function applyRemoteData( category, video ) {

				$scope.category = category;
				$scope.video = video;

				$scope.setWindowTitle( video.speaker + " - " + video.items.title );

			}


			// I load the "remote" data from the server.
			function loadRemoteData() {

				$scope.isLoading = true;

				var promise = $q.all(
					[
						categoryService.getCategoryByID( $scope.categoryID ),
						videoService.getvideoByID( $scope.videoID )
					]
				);

				promise.then(
					function( response ) {

						$scope.isLoading = false;

						applyRemoteData( response[ 0 ], response[ 1 ] );

					},
					function( response ) {

						// The video couldn't be loaded for some reason - possibly someone hacking with the URL.
						$location.path( "/videos/" + $scope.categoryID );

					}
				);

			}


			// --- Define Scope Methods. ------------------------ //


			// ...


			// --- Define Controller Variables. ----------------- //


			// Get the render context local to this controller (and relevant params).
			var renderContext = requestContext.getRenderContext( "standard.videos.detail", "videoID" );

			
			// --- Define Scope Variables. ---------------------- //


			// Get the relevant route IDs.
			$scope.categoryID = requestContext.getParam( "categoryID" );
			$scope.videoID = requestContext.getParamAsInt( "videoID" );

			// I flag that data is being loaded.
			$scope.isLoading = true;

			// I hold the category and video to render.
			$scope.category = null;
			$scope.video = null;

			// The subview indicates which view is going to be rendered on the page.
			$scope.subview = renderContext.getNextSection();

			// --- Bind To Scope Events. ------------------------ //


			// I handle changes to the request context.
			$scope.$on(
				"requestContextChanged",
				function() {

					// Make sure this change is relevant to this controller.
					if ( ! renderContext.isChangeRelevant() ) {

						return;

					}

					// Get the relevant route IDs.
					$scope.categoryID = requestContext.getParam( "categoryID" );
					$scope.videoID = requestContext.getParamAsInt( "videoID" );

					// Update the view that is being rendered.
					$scope.subview = renderContext.getNextSection();

					// If the relevant ID has changed, refresh the view.
					if ( requestContext.haveParamsChanged( [ "categoryID", "videoID" ] ) ) {

						loadRemoteData();

					}

				}
			);


			// --- Initialize. ---------------------------------- //


			// Set the window title.
			$scope.setWindowTitle( "Loading video" );

			// Load the "remote" data.
			loadRemoteData();

            //Set the ready status for the page as soon as this controller is finished
            $scope.status = 'ready';
		}
	);

})( angular, tdv );